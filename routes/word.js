const express = require('express')
const router = express.Router()
const officegen = require('officegen')
const fs = require('fs')
const path = require('path')
const sfn = require('sanitize-filename')
const cns = require('../bin/console')
const dir = require('../bin/dir')

router.post('/', function(req, res, next) {
  const word = officegen('docx')
  const data = JSON.parse(req.body.json)
  const name = sfn(req.body.name)

  const filepath = path.resolve(process.cwd(), 'doc/' + name + '.docx')

  const out = fs.createWriteStream(filepath)
  console.log(cns.chalk.bgRed('准备写入 Word 文档...'))
  console.log('清洗数据...')

  data[0].forEach(word => {
    if (word.type === 'image')
      word.path = path.resolve(process.cwd(), 'img/' + word.path + '.png')
  })
  word.createByJson(data)
  word.generate(out)

  out.on('close', () => {
    console.log('文档写入完成', filepath)
    console.log('整理图片...')

    const picPath = path.resolve(process.cwd(), 'doc/' + name + '/')
    if (!fs.existsSync(picPath)) {
      fs.mkdirSync(picPath)
    } else {
      dir.fnCleanFile(picPath)
    }

    data[0].forEach(word => {
      if (word.type === 'image') {
        console.log(cns.chalk.bgGreen('整理'), cns.chalk.green(word.path))
        fs.renameSync(word.path, path.join(picPath, path.basename(word.path)))
      }
    })
    cns.newFile(filepath)
  })
  res.send({ success: true })
})

router.post('/img', function(req, res, next) {
  let data = req.body.img
  let matches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)
  let img = new Buffer(matches[2], 'base64')

  const filename = path.resolve(process.cwd(), 'img/' + req.body.id + '.png')
  fs.writeFile(filename, img, () => {
    console.log(cns.chalk.bgBlue('缓存'), filename)
  })
  res.send({ success: true })
})

module.exports = router
