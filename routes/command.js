const express = require('express')
const router = express.Router()
const path = require('path')
const cp = require('child_process')

router.get('/folder', function(req, res, next) {
  if (req.query.path) {
    cp.exec(`start "" "${path.resolve(process.cwd(), req.query.path)}"`)
  }
  res.send({ success: true })
})

module.exports = router
