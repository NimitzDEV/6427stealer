const express = require('express')
const router = express.Router()
const officegen = require('officegen')
const fs = require('fs')
const path = require('path')
const sfn = require('sanitize-filename')
const cns = require('../bin/console')
const dir = require('../bin/dir')

router.post('/', function(req, res, next) {
  const data = JSON.parse(req.body.table)
  const name = req.body.name
  const excel = officegen('xlsx')
  const sheet = excel.makeNewSheet()

  console.log(cns.chalk.bgRed('准备写入 Excel 文档...'))

  sheet.name = 'Collected Sheet'

  data.forEach((row, ridx) => {
    sheet.data[ridx] = []
    row.forEach((col, cidx) => {
      sheet.data[ridx][cidx] = col
    })
  })

  const filepath = path.join(dir.docDir, `${name}.xlsx`)
  const out = fs.createWriteStream(filepath)

  out.on('close', () => {
    cns.newFile(filepath)
  })

  excel.generate(out)
})

module.exports = router
