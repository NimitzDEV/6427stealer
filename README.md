# 6427 Stealer

本系统的宗旨： 自动化网页工作，自动化内容处理



## 现有插件

| 文件             | 插件名    |         描述          |
| -------------- | ------ | :-----------------: |
| wxcopy.user.js | 微信文章采集 |  将微信文章采集为 Word 文档   |
| table.user.js  | 表格采集   | 将页面上的表格采集为 Excel 文档 |



## 系统架构

- 服务端使用 express 作为服务器
- 服务端使用 officegen 生成 office 文档 Word 2010+
- 服务端使用 chalk 显示
- 浏览器端依赖现代浏览器和 tampermonkey 插件注入脚本



## 如何在限定地址之外启动插件

由于 tampermonkey 配置的限制，插件预先配置了启用地址。 如果你想在其他页面使用插件，请选择 Tampermonkey 的面板，点开脚本，修改 @match 的地址即可



## 如何安装

1. 服务器依靠 node 环境运行，请到 http://nodejs.cn/ 下载并安装最新的 node 环境
2. 下载安装结束之后，运行 install.bat 初始化环境 （如果你从群里下载的，自带 node_modules 则可以跳过本步骤）
3. 运行结束之后，运行 start.bat 即可开启服务器
4. 开启之后，在浏览器的应用商店安装 tampermonkey
5. 打开浏览器 http://127.0.0.1:3000/ 即可看到插件列表，点击即可安装
6. 如需要更新插件，重新在页面上安装即可

## 如何修改代码

- 服务器入口程序 app.js
- 服务器对应处理程序位于 routes 文件夹中
- 服务器主页位于 public\plugins.html 
- 所有插件文件位于 public 文件夹下的 *.user.js

