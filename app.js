const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const word = require('./routes/word')
const excel = require('./routes/excel')
const command = require('./routes/command')
const app = express()
const cors = require('cors')
const multer = require('multer')
const fs = require('fs')

const con = require('./bin/console')
const dir = require('./bin/dir')

app.use(cors())
dir.fnCheck()
dir.fnCleanFile(dir.imgDir)
con.clean()
con.welcomeScreen()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))

app.use('/word', multer().fields([]), word)
app.use('/excel', multer().fields([]), excel)
app.use('/command', command)
app.use('/', function(req, res, next) {
  res.redirect('/plugins.html')
})

app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use(function(err, req, res, next) {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
  res.send('err! <br />' +  err.message)
})

module.exports = app
