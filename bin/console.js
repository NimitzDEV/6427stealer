const chalk = require('chalk')
const package = require('../package.json')
const path = require('path')

let lineCount = 0
const maxCount = 5

const welcomeScreen = () => {
  console.log(chalk.bgBlue('Welcome to Stealer Content Server'))
  console.log(chalk.bgGreen('版本'), package.version)
  console.log(
    '在浏览器打开地址',
    chalk.bgYellow('http://127.0.0.1:3000'),
    '安装/更新浏览器插件'
  )
  console.log(
    chalk.cyan('==========================================================')
  )
}

const clean = () => console.clear()

const newFile = p => {
  console.log('新文档', chalk.bgRed(path.basename(p)))
  console.log(p)
}

module.exports = {
  welcomeScreen,
  clean,
  newFile,
  chalk,
}
