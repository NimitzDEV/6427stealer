const fs = require('fs')
const path = require('path')
const cns = require('./console')

const root = path.resolve(process.cwd())
const docDir = path.resolve(root, './doc')
const imgDir = path.resolve(root, './img')

const fnCheck = () => {
  fs.existsSync(docDir) || fs.mkdirSync(docDir)
  fs.existsSync(imgDir) || fs.mkdirSync(imgDir)
}

const fnCleanFile = (p) => {
  let files = fs.readdirSync(p)
  files.forEach(file=>{
    console.log(cns.chalk.bgRed('删除!', file))
    fs.unlinkSync(path.join(p, file))
  })
}

module.exports = {
  docDir,
  imgDir,
  fnCheck,
  fnCleanFile
}