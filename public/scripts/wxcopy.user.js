// ==UserScript==
// @name         WX Copy
// @namespace    https://isnimitz.com/
// @version      0.6
// @description  WX Copy by NimitzDEV
// @author       NimitzDEV
// @match        https://mp.weixin.qq.com/s*
// @grant        GM_addStyle
// @grant        GM_registerMenuCommand
// @updateURL    http://127.0.0.1:3000/scripts/wxcopy.user.js
// ==/UserScript==

GM_addStyle(
  `
  .wxCopyPanel {
    position: fixed;
    top: 0; 
    left: 0; 
    background: #000; 
    color: #fff; 
    z-index: 9999;
    max-height: 400px; 
    overflow: auto; 
    font-size: 6px;
    min-width: 100px;
  }
  `
)

function wxCopyProgressPanel() {
  const current = document.getElementById('wxCopyPanel')
  if (current) document.body.removeChild(current)

  const panel = document.createElement('div')
  panel.id = 'wxCopyPanel'
  panel.classList.toggle('wxCopyPanel', true)
  panel.innerHTML = `
    <span id="wxCopyPanelText">请稍后...</span>
  `

  document.body.appendChild(panel)
}

function setAttributes() {
  let imgs = document.querySelectorAll('img')
  for (let i = 0; i < imgs.length; i++) {
    imgs[i].setAttribute('crossOrigin', 'anonymous')
  }
}

async function wxCopyHandler_COLLECT() {
  window.scrollTo(0, 0)

  let result = await wxCopyCollect()
  let fData = new FormData()
  fData.append('json', JSON.stringify(result))
  fData.append('name', document.title)
  await fetch('http://127.0.0.1:3000/word', {
    method: 'POST',
    body: fData,
  })

  document.body.removeChild(document.getElementById('wxCopyPanel'))
}

async function wxCopyCollect() {
  let allP = document.querySelectorAll('div#js_content p')
  let actualPanel = document.getElementById('wxcopy')
  let jsonData = [[]]

  const progressText = document.getElementById('wxCopyPanelText')

  for (let i = 0; i < allP.length; i++) {
    let current = allP[i]
    current.scrollIntoView()

    progressText.innerHTML = `正在采集 ${i}/${allP.length}`

    // back a little bit
    window.scrollTo(0, window.scrollY - 20)

    if (current.querySelectorAll(':scope img').length) {
      try {
        let pid = '' + Date.now() + Math.random() + Math.random()

        let fData = new FormData()

        let imgEle = current.querySelector(':scope img')

        // check natural
        await new Promise(resolve => {
          let retry = 0
          const tmr = setInterval(function() {
            retry++
            if (imgEle.naturalWidth * imgEle.naturalHeight > 1 || retry > 30)
              resolve()
          }, 100)
        })

        if (imgEle.naturalWidth * imgEle.naturalHeight > 100 * 100) {
          fData.append('img', imgSnap(imgEle))
          fData.append('id', pid)
          await fetch('http://127.0.0.1:3000/word/img', {
            method: 'POST',
            body: fData,
          })
          jsonData[0].push({ type: 'image', path: pid })
        }
      } catch (ex) {}
    } else {
      jsonData[0].push({ type: 'text', val: current.innerText })
      jsonData[0].push({ type: 'linebreak' })
    }
  }
  return jsonData
}

function imgSnap(img) {
  var canvas = document.createElement('canvas')
  canvas.width = img.naturalWidth
  canvas.height = img.naturalHeight

  var ctx = canvas.getContext('2d')
  ctx.drawImage(img, 0, 0)

  var dataURL = canvas.toDataURL('image/png')

  return dataURL
}

;(async function() {
  'use strict'
  setAttributes()
  GM_registerMenuCommand('采集此微信文章', function() {
    wxCopyProgressPanel()
    wxCopyHandler_COLLECT()
  })
})()
