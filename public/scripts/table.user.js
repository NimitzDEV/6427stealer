// ==UserScript==
// @name         Table Copy
// @namespace    https://isnimitz.com/
// @version      0.2
// @description  Table Copy by NimitzDEV
// @author       NimitzDEV
// @match        http*://*/*
// @grant        GM_addStyle
// @grant        GM_registerMenuCommand
// @run-at       document-start
// @updateURL    http://127.0.0.1:3000/scripts/table.user.js
// ==/UserScript==

GM_addStyle(
  `
    .wxTableCopy {
      width: 100px;
      color: #fff;
      background: #000;
      padding: 5px;
      font-size: 12px;
      position: fixed;
      top: 0;
      left: 0;
      z-index: 9999;
    }

    .wxTableCopy button {
      font-size: 12px;
      border: none;
    }

    ul.wxTableCopyList {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }

    ul.wxTableCopyList > li {
      display: block;
      color: #fff;
      background: #000;
      font-size: 12px;
      padding: 3px 0;
      cursor: pointer;
    }

    ul.wxTableCopyList > li:hover {
      background: #fff;
      color: #000;
    }

    table[data-wxtable] {
      position: relative;
    }

    .wxTableCommandHook {
      position: absolute;
      padding: 5px;
      background: #1E88E5;
      bottom: 0;
      left: 0;
      right: 0;
    }
  `
)

function wxTableInitPanel() {
  const current = document.getElementById('wxTablePanelContainer')
  if (current) document.body.removeChild(current)

  let panel = document.createElement('div')
  panel.innerHTML = `
    <div id="wxTablePanel" class="wxTableCopy">
      <span>所有表格</span>
      <ul id="wxTableList" class="wxTableCopyList"></ul>
      <button id="wxTableButtonRefresh">刷新</button>
      <button id="wxTableButtonClose">关闭</button>
    </div>
  `
  panel.id = 'wxTablePanelContainer'
  document.body.appendChild(panel)
}

function wxTableHandlerAttach() {
  document
    .getElementById('wxTableButtonRefresh')
    .addEventListener('click', wxTableHandlerSearch, false)

  document
    .getElementById('wxTableButtonClose')
    .addEventListener('click', wxTableHandlerClose, false)
}

function wxTableHandlerClose() {
  const self = document.getElementById('wxTablePanel')
  self.parentNode.removeChild(self)
}

function wxTableHandlerSearch() {
  const tables = document.querySelectorAll('table')
  const list = document.getElementById('wxTableList')
  list.innerHTML = ''
  for (let i = 0; i < tables.length; i++) {
    tables[i].setAttribute('data-wxtable', i)
    let li = document.createElement('li')
    li.onclick = e => {
      wxTableShowTag(i)
    }
    li.innerHTML = '表格 ' + (i + 1)
    list.appendChild(li)
  }
}

function wxTableShowTag(tag) {
  const hooks = document.querySelectorAll('div.wxTableCommandHook')
  for (let i = 0; i < hooks.length; i++) {
    hooks[i].parentNode.removeChild(hooks[i])
  }

  const table = document.querySelector(`table[data-wxtable="${tag}"]`)
  if (!table) return alert('选择的表格已经迷之消失')
  table.scrollIntoView()
  const hook = document.createElement('div')
  hook.classList.toggle('wxTableCommandHook', true)
  hook.innerHTML = `
    <button id="wxTableButtonCollect" data-wxtable="${tag}">采集此表格</button>
    <input type="text" value="表格${tag + 1}" id="wxTablePickName"></input>
  `
  table.appendChild(hook)

  document
    .getElementById('wxTableButtonCollect')
    .addEventListener('click', wxTableCollect, false)
}

function wxTableCollect(evt) {
  const element = evt.target
  const name = element.parentNode.querySelector(':scope > input').value
  if (!name) alert('表格文件名不能为空')

  const data = new FormData()

  const table = []
  const tag = element.getAttribute('data-wxtable')
  const tableElm = document.querySelector(`table[data-wxtable="${tag}"]`)
  const tr = tableElm.querySelectorAll(':scope tr')
  for (let row = 0; row < tr.length; row++) {
    table[row] = []

    const d = tr[row].querySelectorAll(':scope td, :scope th')
    for (let col = 0; col < d.length; col++) {
      table[row].push(d[col].innerText)
    }
  }

  data.append('table', JSON.stringify(table))
  data.append('name', name)

  fetch('http://127.0.0.1:3000/excel', {
    method: 'POST',
    body: data,
  })

  wxTableSuccessMessage(element)
}

function wxTableSuccessMessage(elm) {
  elm.parentNode.innerHTML = `
    <span style="color:#fff">采集完成</span>
  `
  setTimeout(function() {
    const ele = document.querySelector('.wxTableCommandHook')
    ele && ele.parentNode.removeChild(ele)
  }, 2000)
}

;(async function() {
  'use strict'
  GM_registerMenuCommand('采集表格', function() {
    wxTableInitPanel()
    wxTableHandlerAttach()
    wxTableHandlerSearch()
  })
})()
